#To do list / task tracker
This simple web app is my take on the classic to-do list. I built it using mostly ES6 syntax and local storage to store peoples tasks so that they are stored even on refresh of the page.

---

##Contact me
**Email** vcipolla95@gmail.com  

**LinkedIn** https://www.linkedin.com/in/rob-cipolla-4bb728a8/