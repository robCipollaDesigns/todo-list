// Task Class
class Task {
    constructor(title, due, description, id) {
        this.title = title;
        this.due = due;
        this.description = description;
        this.id = id
    }
}

class Storage {

    static getTasks() {

        let tasks;

        if(localStorage.getItem('tasks') === null) {

            tasks = [];

        } else {

            tasks = JSON.parse(localStorage.getItem('tasks'))

        }

        return tasks;

    }

    static addTask(task) {

        const tasks = Storage.getTasks();

        tasks.push(task);

        localStorage.setItem('tasks', JSON.stringify(tasks));

    }

    static deleteTask(id) {

        const tasks = Storage.getTasks();

        tasks.forEach((task, index) => {
           
            if(task.id == id) {

                tasks.splice(index, 1);

            }

        });

        localStorage.setItem('tasks', JSON.stringify(tasks));

    }

}

//UI Class
class UI {

    static displayTasks() {

        const tasks = Storage.getTasks();
        const tasksRow = document.querySelector('#tasks-row');

        if(tasks.length === 0) {

            const noTasks = '<div id="add-tasks-notice" class="col-12 text-center alert alert-dismissible alert-info"><strong>Hmmmmm...</strong> You should add some tasks!</div>';

            tasksRow.innerHTML += noTasks;

        } else {

            tasks.forEach((task) => UI.addTaskToList(task));

        }

    }

    static addTaskToList(task) {

        const tasksRow = document.querySelector('#tasks-row');
        const addTasksNotice = document.querySelector('#add-tasks-notice');

        if(addTasksNotice) {

            addTasksNotice.remove();

        }

        const taskElement = `
            <div class="col-12 col-sm-6 col-md-4 col-lg-3" data-id="${task.id}">

                <div class="card text-white bg-primary mb-3">

                    <div class="card-header">${task.due}<i class="fas fa-trash-alt delete"></i></div>
                    <div class="card-body">

                        <h4 class="card-title">${task.title}</h4>
                        <p class="card-text">${task.description}</p>

                    </div>

                </div>

            </div>
        `;

        tasksRow.innerHTML += taskElement;

    }

    static message(msg, type) {

        //Create message element
        const message = document.createElement('div');
                
        //Add relevant classes
        message.className += `alert alert-dismissible alert-${type}`;

        //Add inner html
        message.innerHTML= msg;

        //Place in DOM and remove after 3 seconds
        document.querySelector('#task-form').insertAdjacentElement('beforebegin', message);

        setTimeout(()=> {
            message.remove();
        }, 3000);

    }

}


//Event: display tasks
document.addEventListener('DOMContentLoaded', UI.displayTasks);

//Event: Add task
document.querySelector('#task-form').addEventListener('submit', (e) => {

    //Prevent default form submit behaviour
    e.preventDefault();

    //Get form values
    const title = document.querySelector('#task-title').value;
    const due = document.querySelector('#due-date').value;
    const descr = document.querySelector('#description').value;

    //Form validation - all fields must be completed
    if(title === '' || due === '' || descr === '') {

        const failMessage = "<strong>Awwww...</strong> Please fill in all fields!";
        UI.message(failMessage, "danger");

    } else {

        //Generate random ID number
        const id = Math.floor(Math.random() * 100000);

        //Create new task object from class
        const task = new Task(title, due, descr, id);

        //Add book to storage
        Storage.addTask(task);

        //Clear field values
        document.querySelector('#task-title').value = '';
        document.querySelector('#due-date').value = '';
        document.querySelector('#description').value = '';

        //Display success message
        const successMessage = "<strong>Woo!</strong> Your task has been added";
        UI.message(successMessage, "success");

        //Add to the UI
        UI.addTaskToList(task);

    }

});

//Event: Remove task
document.querySelector('#tasks-row').addEventListener('click', (e)=>{

    //Get ID of clicked element
    if(e.target.classList.contains('delete')) {
        
        const elID = e.target.parentElement.parentElement.parentElement.getAttribute('data-id');

        //Remove clicked element from UI
        document.querySelector(`[data-id="${elID}"]`).remove();

        //Remove from storage
        Storage.deleteTask(elID);

        const tasks = Storage.getTasks();
        const tasksRow = document.querySelector('#tasks-row');

        if(tasks.length === 0) {

            const noTasks = '<div id="add-tasks-notice" class="col-12 text-center alert alert-dismissible alert-info"><strong>Hmmmmm...</strong> You should add some tasks!</div>';

            tasksRow.innerHTML += noTasks;

        } else {


            tasks.forEach((task) => UI.addTaskToList(task));

        }

    }

});